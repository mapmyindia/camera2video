package com.example.android.camera2video;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CameraFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CameraFragment extends Fragment implements SurfaceHolder.Callback{

    private Camera mCamera;
    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;
    boolean status;
    boolean recording = false;
    Bundle bundle;
    boolean camera_status = false;
    private MediaRecorder mediaRecorder;
    private String  fileuri;
    private Button start;
    private Thread thread;

    public CameraFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CameraFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CameraFragment newInstance() {
        Log.i(Constants.TAG, "");
        return new CameraFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = savedInstanceState;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.i(Constants.TAG, "");
        return inflater.inflate(R.layout.fragment_camera, container, false);
    }

    @Override
    public void onViewCreated(View customView, Bundle savedInstanceState) {
        Log.i(Constants.TAG, "");
        super.onViewCreated(customView, savedInstanceState);
        
        surfaceView = (SurfaceView) customView.findViewById(R.id.surfaceView);
        status = false;
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
         start = (Button) customView.findViewById(R.id.button);
        chooseCamera();
        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                switch(msg.what)
                {
                    case 1 : start.setText("stop");
                        break;
                    case 2 : start.setText("start");
                        break;
                    default:break;
                }
            }
        };

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recording) {
                    try{mediaRecorder.stop(); // stop the recording
                    releaseMediaRecorder();}
                    catch(RuntimeException e)
                    {

                    }
                    thread.interrupt();
                    Toast.makeText(getActivity(), "Video captured!", Toast.LENGTH_LONG).show();
                    start.setText("start");
                    //launchUploadActivity();
                    recording = false;

                }
                else {
                    if (!prepareMediaRecorder()) {
                        Toast.makeText(getActivity(), "Fail in prepareMediaRecorder()!\n - Ended -", Toast.LENGTH_LONG).show();
                        getActivity().finish();
                    }
                    try {
                        mediaRecorder.start();
                        handler.sendEmptyMessage(1);
                    } catch ( Exception ex) {

                    }

                    recording = true;
                    thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(10*1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            try{mediaRecorder.stop(); // stop the recording
                                releaseMediaRecorder();}
                            catch(RuntimeException e)
                            {

                            }
                            handler.sendEmptyMessage(2);
                            // launchUploadActivity();

                            recording = false;

                        }
                    });
                    thread.start();
                }
            }
        });
    }

    private boolean prepareMediaRecorder() {
        Log.i(Constants.TAG, "");
        mediaRecorder = new MediaRecorder();
        mCamera.unlock();
        mediaRecorder.setCamera(mCamera);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
        mediaRecorder.setOutputFile( Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM) + "/"
                + System.currentTimeMillis() + ".mp4");
        fileuri = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM) + "/"
                + System.currentTimeMillis() + ".mp4";
        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    private boolean hasCamera(Context context) {

        Log.i(Constants.TAG, "");
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    private void launchUploadActivity(){

        Log.i(Constants.TAG, "");
        Intent i = new Intent(getActivity(), UploadActivity.class);
        i.putExtra("filePath", fileuri);
    }

    public void chooseCamera() {
        Log.i(Constants.TAG, "");
        int cameraId = findBackFacingCamera();
        if (cameraId >= 0)
        {
            mCamera = Camera.open(cameraId);
            refreshCamera(mCamera);
        }

    }

    public void refreshCamera(Camera camera) {
        Log.i(Constants.TAG, "");
        if (surfaceHolder.getSurface() == null) {
            return;
        }
        try {
            camera.stopPreview();
        } catch (Exception e) {
        }

        try {
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (Exception e) {

        }
    }


    private int findBackFacingCamera() {
        Log.i(Constants.TAG, "");
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    private void releaseMediaRecorder() {
        Log.i(Constants.TAG, "");
        if (mediaRecorder != null) {
            mediaRecorder.reset(); // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            mCamera.lock(); // lock camera for later use
        }
    }

    private void releaseCamera() {
        Log.i(Constants.TAG, "");
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.i(Constants.TAG, "surfaceCreated");
        try {
            if (mCamera != null) {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            }
        } catch (IOException e) {

        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i(Constants.TAG, "surfaceChanged");
        refreshCamera(mCamera);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.i(Constants.TAG, "surfaceDestroyed");
    }

    @Override
    public void onPause()
    {
        Log.i(Constants.TAG, "onPause");
        super.onPause();
        try{
            mediaRecorder.stop(); // stop the recording
            }
        catch(RuntimeException e)
        {

        }
        releaseCamera();
        start.setText("start");
        status = true;
        thread.interrupt();
        recording = false;
    }

    public void onResume() {
        Log.i(Constants.TAG, "onResume");
        super.onResume();
        if (!hasCamera(getActivity())) {
            Toast toast = Toast.makeText(getActivity(), "Sorry, your phone does not have a camera!", Toast.LENGTH_LONG);
            toast.show();
        }
        else
        {
            if(status)
           {onViewCreated(getView(),bundle);}

        }
    }
}
